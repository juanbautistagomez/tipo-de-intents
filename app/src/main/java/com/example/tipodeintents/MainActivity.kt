package com.example.tipodeintents

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button_explicito: Button = findViewById(R.id.button_explicito)
        val button_implicito: ImageView = findViewById(R.id.implicito)

        button_explicito.setOnClickListener {
            val intent = Intent(this, Second::class.java)
            startActivity(intent)
        }

        button_implicito.setOnClickListener {
            val uri= Uri.parse("https://bitbucket.org/juanbautistagomez/")
            val intent = Intent(Intent.ACTION_VIEW,uri)
        startActivity( intent)}

    }
}